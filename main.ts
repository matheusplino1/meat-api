import {Server} from './server/server';
import {usersRouter} from './users/users.router';

const server = new Server();

server.bootstrap([usersRouter]).then((server) => {
	console.log(`Servidor rodando no endereço ${server.application.address()}`);
}).catch(error => {
	console.log('Aconteceu algum erro');
	console.log(error);
	process.exit(1);
});
